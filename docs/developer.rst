Running tests:
$ source /opt/django/project_name/venv/bin/activate
$ cd /tmp/project_name/project_name/
$ coverage run --include=apps/*.* manage.py test --settings=project_name.settings.test 
$ coverage report --include=apps/*.*
$ coverage html --include=apps/*.*

