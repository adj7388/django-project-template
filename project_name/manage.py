#!/usr/bin/env python
import os
import sys
from apps.utils.env import get_env_variable

if __name__ == "__main__":
    settings_module = "{{ project_name }}.settings.{}".format(get_env_variable('DJANGO_ENV'))
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", settings_module)

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
